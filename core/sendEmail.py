#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""=================================================
@Project -> File   ：dingtalkclockin -> sendEmail.py
@Author ：后门别棍
@Date   ：2021/3/31 12:10
=================================================="""
import os
import smtplib
from log_config.logger import Log
from email.header import Header
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.utils import parseaddr, formataddr
from email.mime.multipart import MIMEMultipart
from setting.config import SEND_USER, SEND_PWD, RECEIVERS, SMTP_SERVER, SMTP_PORT, IMAGE_PATH
log = Log().Logger


# 格式化邮件地址
def format_ddr(s):
    name, ddr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), ddr))


def send_imag_email(title='默认主题', data='默认内容', path=IMAGE_PATH, image_name=None, link='超链接'):
    """
    :rtype: bool
    :param title: 邮件主题
    :param data：邮件内容
    :param path：图片路径
    :param image_name：图片名称
    :param link：超链接
    """
    smtp_server = SMTP_SERVER
    email_account = SEND_USER
    account_password = SEND_PWD
    to_mail = RECEIVERS
    try:
        # 构造一个MIMEMultipart对象代表邮件本身
        msg = MIMEMultipart()
        # Header对中文进行转码#
        msg['From'] = format_ddr(f'管理员 <{email_account}>')
        msg['To'] = ','.join(to_mail)
        msg['Subject'] = Header(title, 'utf-8')
        msg.attach(MIMEText(data, 'html', 'utf-8'))
        # 二进制模式读取图片
        if image_name:
            with open(path + '/' + image_name, 'rb') as f:
                msg_image = MIMEImage(f.read())
                # 使用MIMEImage类构造图片并添加到msg对象
                mail_msg = f"""
                <p><a href={link}>{link}</a></p>
                <p>详情：</p>
                <p><img src="cid:image1"></p>
                """
                msg.attach(MIMEText(mail_msg, 'html', 'utf-8'))
                # 定义图片ID，根据ID在HTML里获取图片
                msg_image.add_header('Content-ID', 'image1')
                msg.attach(msg_image)
        s = smtplib.SMTP_SSL(smtp_server, SMTP_PORT)
        s.login(email_account, account_password)
        s.sendmail(email_account, to_mail, msg.as_string())
        s.quit()
        log.info(f'邮件发送成功')
        return True
    except smtplib.SMTPException as e1:
        log.error(f'邮件发送失败\nmsg:{e1}')
        return False
    except FileNotFoundError as e2:
        log.error(f'找不到图片，检查地址是否正确\nmsg:{e2}')
        return False
    except Exception as e3:
        log.error(f'参数错误，或者网络问题\nmsg:{e3}')
        return False


if __name__ == "__main__":
    send_imag_email('主题', '内容:', link='https://www.baidu.com')
