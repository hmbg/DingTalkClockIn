#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""=================================================
@Project -> File   ：DingTalkClockIn -> clockIn
@Author ：后门别棍
@Date   ：2021/3/29 10:24
=================================================="""

from setting.config import *
from log_config import logger
import uiautomator2 as u2

# 实例化log
log = logger.Log().Logger


class ConnectConfig:
    def __init__(self, device_wifi: str = DEVICE_WIFI):
        """
        :param device_wifi: 设备IP
        """
        # cmd命令返回数据
        data = os.popen('adb devices').read()
        # 匹配设备号的正则表达式
        pattern = re.compile(r'(?<=\n)\S*(?=\t)')
        # 获取到的设备号
        DEVICE_NO = pattern.search(data)
        if DEVICE_NO != None:
            self.device_no = DEVICE_NO.group()
        self.device_wifi = device_wifi
        if not self.device_wifi:
            log.warning('DEVICE_WIFI不可为空')

    def connecttype(self):
        """连接设备"""
        try:
            drive = u2.connect_usb(self.device_no)
            log.debug(f'device_no={self.device_no}')
        except Exception as e:
            log.error(f'USB连接设备失败，原因：{e}，正在尝试使用WiFi连接设备')
            try:
                drive = u2.connect_wifi(self.device_wifi)
            except Exception as a:
                log.error(f'设备连接失败，原因：{a}')
                raise a
            else:
                log.info('WIFI连接设备成功')
                log.debug(f'详细信息：{drive.info}')
                return drive
        else:
            log.info('USB连接设备成功')
            log.debug(f'详细信息：{drive.info}')
            return drive
