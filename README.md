# 钉钉自动打卡 V1.0.5

### 注意事项
    1.python版本不能低于3.6
    2.首次使用需要手动允许钉钉读、写、定位权限（手机设置）
    3.设备必须为常用打卡手机（不支持安全认证）
    4.因为连接方式使用的是wifi，所以有时候连接会不稳定导致定位不到元素，属于正常情况，后续版本会增加智能判断连接方式

### 使用说明

#### 1.安装依赖库
    pip install uiautomator2
    如果安装失败使用下面这条命令
    pip install -i https://pypi.tuna.tsinghua.edu.cn/simple/ uiautomator2

#### 2.手机安装ATX
手机开启开发者模式，开启USB调试，用USB连接PC，打开CMD，使用“adb devices”命令检查手机是否连接成功，
如果连接成功，使用“python -m uiautomator2 init”安装ATX手机客户端

#### 3.修改配置文件
    DINGTALK_ACCOUNT   需要打卡的手机号
    DINGTALK_PASSWORD  账号密码
    DEVICE_NO          移动设备ip
    CLOCK_TYPE         打上班卡还是下班卡（参数只能为“上班”或者“下班”）（已弃用）

#### 4.启动
运行项目根目录下run.py

## 更新记录
    V1.0.0：打卡基本功能实现
    V1.0.1：删除CLOCK_TYPE参数，新增自动判断打卡时间段，是打上班卡还是下班卡，不在打卡时间段不允许打卡
    V1.0.2：新增打卡完成后截图并将截图发送到指定邮箱
    V1.0.3：新增DEVICE_WIFI参数，智能选择连接方式
    V1.0.4：优化代码，只需配置设备IP，系统自动识别设备号
    V1.0.5：钉钉更新：登录时需要勾选隐私协议


