#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""=================================================
@Project -> File   ：DingTalkClockIn -> config
@Author ：后门别棍
@Date   ：2021/3/29 10:20
=================================================="""

import os
import re

# 获取项目路径
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 截图存放位置
IMAGE_PATH = os.path.join(BASE_PATH, 'screenshot')
# 定义日志文件的路径
LOG_PATH = os.path.join(BASE_PATH, 'logs')
# 日志级别
LOG_RANK = 'debug'.upper()
# 需要打卡钉钉账号
DINGTALK_ACCOUNT = '17638832598'
# 钉钉密码
DINGTALK_PASSWORD = 'pwd'
# 设备ip（局域网）
DEVICE_WIFI = '192.168.10.216'
# 邮件发送人
SEND_USER = "735274922@qq.com"
# 邮箱密码或密钥（秘钥，不是密码）
SEND_PWD = 'gjjdehlaixsjgggbfb'
# 邮箱收件人
RECEIVERS = '735274922@qq.com'
# 邮箱服务器地址
SMTP_SERVER = 'smtp.qq.com'
# 邮箱服务器端口
SMTP_PORT = 465
