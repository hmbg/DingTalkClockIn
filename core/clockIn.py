#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""=================================================
@Project -> File   ：DingTalkClockIn -> clockIn
@Author ：后门别棍
@Date   ：2021/3/29 11:24
=================================================="""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from core.connect import ConnectConfig
from core import sendEmail
from log_config import logger
from setting.config import *
import time
# 实例化log
log = logger.Log().Logger


class ClockIn:
    """实现钉钉自动打卡功能"""

    def __init__(self):
        """
        wait_time:隐式等待时间
        """
        self.living = ConnectConfig()
        self.d = self.living.connecttype()
        self.wait_time: int = 20
        self.packagename = 'com.alibaba.android.rimet'
        self.clear = False
        # 设置隐式等待
        self.d.implicitly_wait(self.wait_time)
        # 初始化打卡状态为0
        self.clock_status = 0

    def openApp(self):
        """启动app"""
        while not self.d.info.get('screenOn'):
            # 点亮屏幕
            self.d.screen_on()
            log.info('屏幕已点亮')
            # self.d.swipe(0.5, 0.7, 0.5, 0.3, 0.03)
            # log.info('手机已解锁')
            try:
                self.d.app_start(self.packagename, wait=True)
            except Exception as e:
                log.error(f'打开app失败，检查包名是否正确{e}')
                raise
            else:
                log.info('已打开app')
                time.sleep(5)
        else:
            try:
                self.d.app_start(self.packagename, wait=True)
            except Exception as e:
                log.error(f'打开app失败，检查包名是否正确{e}')
                raise
            else:
                log.info('已打开app')

    def login(self):
        # 清除手机号
        self.d(resourceId='com.alibaba.android.rimet:id/et_phone_input').clear_text()
        # 输入手机号
        self.d(resourceId='com.alibaba.android.rimet:id/et_phone_input').send_keys(DINGTALK_ACCOUNT)
        # 输入密码
        self.d(resourceId='com.alibaba.android.rimet:id/et_pwd_login').send_keys(DINGTALK_PASSWORD)
        # 勾选隐私协议
        self.d(resourceId="com.alibaba.android.rimet:id/cb_privacy").click()
        # 点击登录按钮
        self.d(resourceId='com.alibaba.android.rimet:id/btn_next').click()
        if self.d(resourceId="com.alibaba.android.rimet:id/my_avatar").wait():
            log.info('登录成功')

    def runClockIn(self):
        try:
            # 关闭app
            self.d.app_stop(self.packagename)
            # 打开钉钉
            self.openApp()
            if self.d(text='现在去写').wait(timeout=5):
                self.d(text='等会再写').click()
            # 判断是否已登录
            if self.d(resourceId="com.alibaba.android.rimet:id/et_phone_input").wait(timeout=5):
                log.info('账号未登录，正在登录中')
                self.login()
            else:
                log.info(f'当前已有账号登录')
                log.info('正在退出登录')
                # 进入我的
                self.d(resourceId='com.alibaba.android.rimet:id/home_app_item')[4].click()
                # 点击设置
                self.d(text='设置').click()
                # 点击退出登录
                self.d(resourceId='com.alibaba.android.rimet:id/setting_sign_out').click()
                # 点击弹窗确认
                self.d(resourceId='android:id/button1').click()
                log.info('正在登录中')
                self.login()
            if self.d(text='现在去写').wait(timeout=5):
                self.d(text='等会再写').click()
            # 进入主页
            self.d(resourceId='com.alibaba.android.rimet:id/home_app_item')[2].click()
            # 进入考勤打卡
            self.d(text='考勤打卡').click()
            now_time = time.strftime("%H:%M", time.localtime())
            if now_time <= "09:33":
                # 开始打上班卡
                if not self.d.xpath(
                        '//*[@resource-id="191390891580"]/android.view.View[2]/android.view.View[2]/android.view.'
                        'View[1]/android.widget.Image[1]').wait(timeout=5):
                    self.d.xpath('//*[@resource-id="__react-content"]/android.view.View[1]/android.view.View[1]/android.'
                                 'view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.'
                                 'View[3]/android.view.View[1]/android.view.View[1]/android.view.View[1]').click()
                    log.info('上班打卡成功')
                else:
                    log.info('已打过上班卡')
                # 修改上班卡状态为1
                self.clock_status = 1
            elif "19:30" <= now_time <= "21:00":
                # 开始打下班卡(不加班)
                if not self.d.xpath(
                        '//*[@resource-id="191390891581"]/android.view.View[2]/android.view.View[2]/android.'
                        'view.View[1]/android.widget.Image[1]').wait(timeout=5):
                    self.d.xpath('//*[@resource-id="__react-content"]/android.view.View[1]/android.view.View[1]/android.'
                                 'view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.'
                                 'View[3]/android.view.View[1]/android.view.View[1]/android.view.View[1]').click()
                    log.info('下班打卡成功(不加班)')
                else:
                    log.info('已打过下班卡(不加班)')
                # 修改上班卡状态为1
                self.clock_status = 1
            elif now_time > "21:00":
                # 开始打下班卡(加班)
                if not self.d.xpath(
                        '//*[@resource-id="191390891581"]/android.view.View[2]/android.view.View[2]/android.'
                        'view.View[1]/android.widget.Image[1]').wait(timeout=5):
                    self.d.xpath('//*[@resource-id="__react-content"]/android.view.View[1]/android.view.View[1]/android.'
                                 'view.View[1]/android.view.View[1]/android.view.View[2]/android.view.View[1]/android.view.'
                                 'View[3]/android.view.View[1]/android.view.View[1]/android.view.View[1]').click()
                    log.info('上班打卡成功(加班)')
                else:
                    log.info('已打过下班卡(加班)')
                # 修改上班卡状态为1
                self.clock_status = 1
            else:
                self.d.app_stop(self.packagename)
                log.error(f'当前时间不允许打卡，当前时间{time.strftime("%H:%M", time.localtime())}')
            if self.clock_status:
                log.info('正在截图中')
                time.sleep(10)
                # 定义截图名称
                screenshot_name = time.strftime("%Y_%m_%d_%H_%M_%S") + '.png'
                # 截图
                self.d.screenshot(os.path.join(BASE_PATH, "screenshot\\") + screenshot_name)
                # 关闭app
                self.d.app_stop(self.packagename)
                # 发送截图邮件
                try:
                    sendEmail.send_imag_email('打卡截图', image_name=screenshot_name)
                except Exception:
                    log.warning('邮件发送失败')
                else:
                    log.info('邮件发送成功')
            else:
                self.d.app_stop(self.packagename)
            self.d.screen_off()
        except Exception as ae:
            log.error(f'运行失败，原因：', ae)
            # 定义截图名称
            screenshot_name = time.strftime("%Y_%m_%d_%H_%M_%S") + 'error.png'
            # 截图
            self.d.screenshot(os.path.join(BASE_PATH, "screenshot\\") + screenshot_name)
            # 关闭app
            self.d.app_stop(self.packagename)
            # 发送截图邮件
            sendEmail.send_imag_email('打卡截图', image_name=screenshot_name)
            self.d.app_stop(self.packagename)
            self.d.screen_off()
        log.info('\n' + '*' * 90 + '结束' + '*' * 90)


if __name__ == '__main__':
    run = ClockIn()
    run.runClockIn()
