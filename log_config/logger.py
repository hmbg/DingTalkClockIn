#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""=================================================
@Project -> File   ：DingTalkClockIn -> logger
@Author ：后门别棍
@Date   ：2021/3/29 10:24
日志封装
=================================================="""
from setting.config import LOG_PATH, LOG_RANK
import logging
import time
import os


class Log(object):
    def __init__(self, path=LOG_PATH, level=LOG_RANK, log_name='TEST'):
        """
        :param path: log文件存放路径
        :param level: log等级：DEBUG、INFO、WARNING、ERROR
        :param log_name: log文件名称（自带时间）
        """
        self.__log_path = path
        self.__level = level
        self.__log_name = log_name
        self.__path = os.path.join(self.__log_path, f'{self.__log_name}-{time.strftime("%Y-%m-%d")}.log')
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(self.__level)

    def __ini_handler(self):
        """初始化处理"""
        if not os.path.exists(LOG_PATH + '/'):
            os.makedirs(LOG_PATH)
        file_handler = logging.FileHandler(self.__path, encoding='utf-8')
        stream_handler = logging.StreamHandler()
        return stream_handler, file_handler

    def __set_handler(self, stream_handler, file_handler, level):
        """决定处理程序级别并添加到收集器"""
        stream_handler.setLevel(level)
        file_handler.setLevel(level)
        self.__logger.addHandler(stream_handler)
        self.__logger.addHandler(file_handler)

    @staticmethod
    def __set_formatter(stream_handler, file_handler):
        """决定日志格式"""
        formatter = logging.Formatter('[%(asctime)s.%(msecs)03d]-[%(levelname)s]-[%(filename)s:%(lineno)d]:%('
                                      'message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
        stream_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)

    @staticmethod
    def __close_handler(stream_handler, file_handler):
        """关闭处理程序"""
        stream_handler.close()
        file_handler.close()

    @property
    def Logger(self):
        """集合并返回日志程序"""
        self.__logger.handlers.clear()
        if not self.__logger.handlers:
            stream_handler, file_handler = self.__ini_handler()
            self.__set_handler(stream_handler, file_handler, self.__level)
            self.__set_formatter(stream_handler, file_handler)
            self.__close_handler(stream_handler, file_handler)
        return self.__logger


if __name__ == '__main__':
    log = Log().Logger
    log.debug('debug')
    log.info('info')
    log.warning('warning')
    log.error('error')
